ArmA 3 Clan Rules
#################

.. note::
	All players are required by the community and the management to read through this agreement and the rules when joining the 11th Marine Expeditionary Unit. You are responsible for checking this rules list for any updates it may have which is denoted by the "Last Updated" timestamp. Rules may be updated at any time, it is your job to keep updated with them, When joining the server it's implied that you accept these terms as well as you follow them.

	
1.0 General rules & Overview
----------------------------
11th Marine Expeditionary Unit is a semi-serious role-play clan, we aim as a community to bring semi-realistic military simulations. We as the clan aren't here to represent a 100% accuracy of what the United States Marine Corps do or have. This message is a warning to you that are here to complain that the "camouflage" pattern doesn't 100% match the real once they use, or vehicles aren't a 100% match to what they have in real life. Yes, we do understand that this clan is based on the United States Marine Corps but at the end, we're all here to play as a group and have fun together.

11th Marine Expeditionary Unit is a community, there is no one specifically in-charge over the group, the community itself is in-charge over the group. Meaning your voice is a part of all its decisions. If you wish for changes to happen you have to speak up as a member of the community, same goes for in-game changes such as missions, vehicles, group layouts, etc. Our missions are available to the public meaning you as a member of the community can change, edit or even build your own missions for the community. All though we in the staff team will make sure there are weekly events, we can not guarantee a unique and custom mission on a weekly basis, that all depends on whom are willing to help out or have built a new mission for the community to be deployed at.

**General Rules:**

+ The most important rule is to always use Common Sense.
+ As a member of this community, you must understand that this is a semi-serious role-play clan.
+ You MUST have a working microphone.
+ Your in-game name MUST be your profile name listed on our website.
+ Whilst being on our game server you MUST be connected to our Teamspeak 3 server and present in our in-game channel with the plugin activated!
+ If you disconnect at any point in a combat/role-play situation you will be punished accordingly, this does NOT include game crashes.
+ Be respectful and reasonable in OOC, OOC is used for out of character purposes. In character, you can say and do whatever you want to an extent, If a player finds it offensive and asks you to stop you MUST do so.
+ Disrespect is allowed in character if they offend you ask them to stop in local chat. If they don't stop call an admin. DO NOT TAKE MATTERS INTO YOUR OWN HANDS.


1.1 Deployment central
----------------------
Our 'Deployment Central' is a great tool developed by 'Ben Sherman', it was originally designed for a whitelisting purpose but became so much more over the time. It's a great tool for all our members to keep track of their statistics, squads, upcoming events and so much more. The deployment central are still in an early version, there may or may not be bugs occurring from time to time. If you should encounter an error message or a bug please send a message to '123B3n' on the website.

**Guidelines:**

+ You may **NOT** use the message system as a social center, only use it for community or clan related content.
+ You may **NOT** advertise other clans or communities on any section of the 11th Marine Expeditionary Unit.
+ You may **NOT** manipulate any bugs or errors.


1.2A Account Inactivity
-----------------------
Since you are a member of this unit you are expected to uphold your account status as active, this can only be done if you login to our website from time to time. If you shouldn't login for a period of **2 months** your account status will automatically be changed to inactive if your account status becomes inactive our staff team will attempt to contact both via Steam & email. Once they successfully contacted you, you will be given a **7 days** period to change your status from inactive to active in which can only be done through our website. Please keep in mind that just because you are active in-game doesn't change your status on our unit as the in-game server cannot calculate the current date and time in which it cannot update the request correctly and therefore you have to login to our website from time to time in order to keep your account status as active.


1.2B Mandatory Events
---------------------
Each week our unit create a custom and unique mission which we call "Deployment", these events are Mandatory for each and every one of us. However, even though we expect you to participate in these events certain things in real life may come in the way, for example, vacations, work, school, and so much more. If this should happen to you it is your job to go to our website and mark the specific event as you cannot participate. This is so that we aren't waiting for you on the briefing before we can start, this is something that most people don't seem to understand and it delays our events with hours and frankly, it's annoying and even disrespectful from you as a member. If you for some reason shouldn't participate and you haven't marked the event as "Not Participating" you will receive a strike from our systems (**Section 1.5**), strikes can be useful for smaller violations such as this one, or even other things such as in-game trolling (**Section 1.2C**). Something else to keep in mind is that if you have marked an event as "Not Participating" and yet you are playing another game on steam and someone sees this you will be issued a **warning**, this example also goes for those who haven't marked the event. **Warnings** are only given once if it happens twice your account will be **permanently suspended** from our unit.

We get that this seems rude but after all you signed up yourself to this unit and at the same time marked that you've read and understood these rules or are about to, if you for some reason no longer want to be a member or take a break from this unit you can do so yourself at any time by simply disabling your account under "Account settings". We aren't forcing you to be a member of this unit, you've completely done this yourself.


1.2C Trolling & Misbehavior
---------------------------
We've lately noticed a spike in-game on Trolling & Misbehavior and this is unacceptable, something to keep in mind is that our strike system wasn't just designed for if you missed an event. The strike system can also be used for such as this subject, whilst our admins haven't fully utilized the system before but from here on out are expected to fully utilize the system. This means, if you are bound to be trolling in-game or misbehaving you can and will expect a strike to be given on your behalf. Please keep in mind that even though an admin has the right to issue you a strike, they also have the right to remove you from the session with either a kick or temporary ban depending on what they feel is necessary. Last admin meeting we had, we decided that not just will we be utilizing the strike system to it's fullest we will also be demoting people for trolling. If you should be the minimum rank required for your current role and you are demoted you will lose said role. Awards and ribbons will also not be given out to those who have been trolling for the current session, we are NOT limited to only one of these subjects, you can be issued all at once!

**Trolling Classifications:**

+ Ruining the role-playing perspective (Excluded trolling in-character)
+ Firing your weapon on the base without authorization (Such as firing RPG's, grenade launchers, etc)
+ Antecedently throwing grenades on the base several times (One time is an accident, more than that isn't)
+ Shooting friendlies because their AFK or ignoring you
+ Killing friendlies with chairs or any other movable object (Keep in mind they are taking damage if you walk into them with an object picked up)
+ And so on...

**Misbehavior Classifications:**

+ Teamkilling (Not including accidents or in-character blue on blue)
+ Completely ignoring admins decisions
+ Goes complete 12 years old in-game because you don't like what people said or did or even an admin told you to do.


1.3 Mandatory service period
----------------------------
This a fancier word of whitelisting. Our mandatory service period application consists of a few and fairly simple questions, none of the questions are related to real life. The application itself is role-played, basically what we're looking for is just to see if you know the definition of role-playing as well as we need to verify that your steamid64 is correct or else you will not be able to join our server. We are also looking into your steam history such as VAC bans and if you have a legit copy of ArmA 3 otherwise the whitelisting is pointless. The reason we're looking into VAC bans is fairly simple, we don't want any hackers on our servers, we do not say your going to be denied because you have a year old VAC ban or anything like that, but if you have a VAC ban that is 2 days old then yes you will be denied.

**Guidelines to get accepted:**

+ Own a legal copy of ArmA 3.
+ Do **NOT** have a VAC ban earlier than 6 months period.
+ Do **NOT** enter any bullshit information, answer the questions as it is.
+ Do **NOT** enter an unrealistic or famous role-play name, i.e John Cena, Michael Jackson.
+ Just answer the questions as professional and simple as possible, we're not in the need of your life story.


1.4 Weekly deployments
----------------------
11th Marine Expeditionary Unit hosts weekly deployments, this means that we in the community expects you to participate in those weekly deployments. However, we can't force you to be available for all deployments but we expect you to. We in the community try and fit the weekly deployments for both US, EU members, this means that the time for deployment is usually between 1-2PM CST and 7-8PM GMT. The clock on the website shows your current time zone and not the server time zone. Something to keep in mind is that all deployments listed have been converted over to your local time zone, this means that if a deployment is listed for example 10 PM the time for that deployment is 10 PM your local time, we've done so to ease of your shoulders with conversions and different time zones which can be frustrating and confusing from time to time. If you for some reason shouldn't be able to participate in one of the weekly deployments you **MUST** go to the upcoming deployment page, click on the event and mark it as you can't participate. You will be asked to enter some information why you can **NOT** participate. The reason can **NOT** be for example (Busy/I don't feel like joining or playing today) such reasons are **NOT** valid. For reasons like (I'm on vacation/I'm on a wedding, etc) such reasons are **valid reasons**. If you should fail to mark the event as LOA you will receive one strike on your account, to understand more what the strikes are read through **section 1.5**.


1.5 Strike system
-----------------
We've implemented a strike system to our community, meaning if you do something extremely dumb or stupid you'll receive a strike. Strikes are a way for us to punish our members when breaking server rules and so on. Each account can get a maximum of fourth strikes if you should receive one strike, that strike will disappear after sixty days. If you should receive up to four strikes you will be removed from this unit as it means you are not interested or even for that matter taking care of your account. We've informed everyone several times how to avoid receiving strikes, most common one is the mandatory events. It takes two minutes to say that you are not able to participate yet no one even bothers about it and do not even bother about saying you don't receive notifications, we are posting everywhere about mandatory events, twitter, steam groups and so on. They're all on the same date, same time, more or less every week.

**Useful Information:**

+ If you should receive four strikes you able to re-apply to enter this unit, but the stuff you had before will not be refunded!
+ A user may only receive one strike at a time, an admin may not issue four strikes at once.
+ Each strike disappears sixty days after it was received.


2.0 Squad & assignments
-----------------------
Once your mandatory service period has been approved you'll be assigned to next available squad, the assignment will be based on what you've entered as the desired duty during your application. If you, for example, chose anything under "Other assignments" you will not get your own position within a squad, this is because we can not guarantee that for example "Pilots" are available on all of our missions. Therefore if you should be a "Pilot" and there are no pilots available on the mission you have to choose the next available role within a squad. It's always recommended that you speak to a squad leader beforehand so you know if there are any slots available within that squad, the squad leaders will always know if he or she has an open spot within their squad, it could be an empty slot it could also be that one of his or her squad members are not able to attend the current mission.

If you wish to change your current role or squad you can do so by talking to either the squad leader, team leader or platoon leaders. It's very important that you follow that chain of command order, however, if for some reason you should want to switch squad because you and your squad leader don't like each other or for some other reason you don't want to go to him first, you can always go above him or her, i.e the team leader. I think you understand by now.


2.1 Chain of command
--------------------
If you are a member of the 11th Marine Expeditionary Unit you are expected to follow the chain of command, if you are in a squad you report directly to your Squad Leader even if you hear a direct order from the platoon leader you do not execute till you are given the go-ahead from your squad leader. You are expected to address your leaders by their **RANK** or **SIR**, same goes for those who are higher ranked than you.


2.2 Ranks & Ranking
-------------------
You may be confused whilst in-game as you may see everyone with the rank **"Colonel"**, this rank is invalid and you can ignore it. The reason for this rank occurring in-game is because the Marine Corps uses different ranks and far more ranks than ArmA 3 can have themselves, even though we can adjust the rank structure to its real ranks, we can still not have more ranks than ArmA 3 are built for which by default is 7 whilst the Marine Corps has 27 ranks. The current rank of all our members should be displayed in front of their name for example "Pvt - B. Sherman". In which case the rank would be "Private". For more information related to the Marine Corps ranks `click here <http://www.marines.mil/Marines/Ranks.aspx>`_.

Ranking your soldier in our community is fairly simple, just show courage and dedication. We do not give out free ranks or for that matter hand them out like a Christmas card, if you wish to obtain a new rank you may do so by showing activity in-game. Just do as your told, follow orders and you will receive a new rank eventually. Ranks are usually given out by your squad leader as he or she recommends the recommendations to the platoon leader and from there, he or she decides if this person should or shouldn't receive a new rank or not. Ranks are given in the structure form, meaning you can **NOT** jump from **"Private"** to **"Lieutenant"**.


2.3 Roles & training
--------------------
We've already mentioned the weekly deployments when you are deployed you are expected to know the duty you've chosen. Even though you may change your assignment at any time, you can **NOT** get deployed as a medic when you do not know how the medic class works or what to do. Therefore we have the server open at all times even when there are no deployments active in which you have the opportunity to try out new things, prepare and get ready for either your new role or even test out new roles that you are planning on switching to eventually. You are practically free to do whatever you want to do whilst there are no deployments active on the server if you wish to start training in a role that you have no clue about and don't know where to go from there you can always ask any of your squad leaders if they can help out or point you in the right direction. We have people who can get you going as well as we can get you set up and trained in that specific role that you are currently trying to train for.

We do not force our members to join any training meaning it's up to you as a member to ensure you are fit for the role that you're assigned to, if you wish to get trained by someone you can do so by asking your squad leaders. However, keep in mind that your squad leader can force you to be trained if he or she feels like you aren't fit for the role you're assigned to.


3.0 Mission Framework
---------------------
Our mission framework is **NOT** available to the public, meaning only a few people within the community have access to the whole framework. Even though we provide the template so that anyone within the community can create their own mission for the server, the template doesn't contain the framework itself. Once you've as a member created a template for the community you have to send it to either **'123B3n'** or **'RKupa'** in which they will set it up for the framework itself.

Our mission framework is quite unique comparing to most other communities, for one we have the ability to throw out Zeus, whilst Zeus is still active in-game for our admins it's rarely used, the reason for this is because our mission framework has the ability to generate everything on the spot meaning if you create a template you don't have the setup a bunch of groups and script them to do certain things. Our mission framework enhances the AI, meaning the AI now acts more as a player than an AI itself, they listen, communicate (if they have a radio of course), they flank you if you sit still at one place for a longer period, they peak corners. Overall our mission framework has a lot to it.