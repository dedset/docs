Welcome to Sunrise Roleplay's documentation!
############################################

This documentation covers rules and features of the `Sunrise Roleplay Community`_.
This resource uses both markdown and Sphinx in .txt documents.
Use the side bar for navigation.

.. _Sunrise Roleplay Community: http://sunrise-roleplay.eu/
.. _English Server; https://live.sunrise-roleplay.eu/
.. _Estonian Server; https://live.sunrise-roleplay.eu/
.. _Forums: https://forums.sunrise-roleplay.eu/
.. _gitlab: https://gitlab.com/sunrise-roleplay/
.. _discord: https://discord.gg/eANRxmX

.. toctree::
   :maxdepth: 2
   :caption: Community Guidelines:

   rules/forums
   rules/forumprivacy
   rules/forumterms
   
.. toctree::
   :maxdepth: 2
   :caption: FiveM Guidelines:

   rules/server-rules
   rules/weapon-roleplay

Links
==================

* `Forums`_
* `Discord`_
* `Gitlab`_
* `English Server`_
* `Estonian Server`_