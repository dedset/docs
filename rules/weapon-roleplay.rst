#############
Weapon Roleplay
#############

This document covers which weapons are legal and explains what type of action, as well as when and where, you can take out all types of weaponry for use in combat RP. Should this not be complied with during roleplay it will be classed as failRP and will be dealt with accordingly.

.. danger:: Lap-carrying: Lap-carrying a weapon means that the weapon is essentially in your hand or lap while in the car. If a player is within reasonable range to see into the vehicle, it’s important to make the weapon known to other players. This should be done with a /me. If you opt to not lap-carry the weapon it must be retrieved from a glovebox or compartment with a /me. For example, 

        * Lap-carry: /me has a pistol on his lap.
        * Retrieval: /me opens the glovebox and pulls out a pistol.

        Tinted windows are an exception to this rule. If the windows in the vehicle are blacked out, it is reasonable that the other player would not be able to see you pull the weapon. This is void if the windows are rolled down or smashed. 


Melee Weapons
=============

+-----------------------------------------------+-----------------------------------------------+
| **Legal**                                     | **Illegal**                                   |
+-----------------------------------------------+-----------------------------------------------+
| Knife                                         | Knuckle Dusters                               |
+-----------------------------------------------+-----------------------------------------------+
| Dagger                                        | Battle Axe                                    |
+-----------------------------------------------+-----------------------------------------------+
| Switchblade                                   |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Bottle                                        |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Machete                                       |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Hatchet                                       |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Hammer                                        |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Bat                                           |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Golf Club                                     |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Pool Cue                                      |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Wrench                                        |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Flashlight                                    |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Nightstick (Police Only)                      |                                               |
+-----------------------------------------------+-----------------------------------------------+

* Most don’t require a /me to be removed from a bag or article of clothing.
* Baseball bats, Golf Clubs, and Pool Cues are the exception.They are too big to pull from a pocket and will require a /me to bring the weapon into fair play. 
* Can be carried on a motorbike





Pistols
=======

+-----------------------------------------------+-----------------------------------------------+
| **Legal**                                     | **Illegal**                                   |
+-----------------------------------------------+-----------------------------------------------+
| Taser                                         | AP Pistol                                     |
+-----------------------------------------------+-----------------------------------------------+
| Flare Guns                                    |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Sns Pistol                                    |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Sns Pistol MK.2                               |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Pistol                                        |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Pistol MK.2 (class 3)                         |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Heavy Pistol                                  |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Pistol .50 (class 3)                          |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Double Action Revolver                        |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Heavy Revolver (Rare)                         |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Marksman Pistol (Rare)                        |                                               |
+-----------------------------------------------+-----------------------------------------------+
| Combat Pistol (Police Only)                   |                                               |
+-----------------------------------------------+-----------------------------------------------+

* Pistols do not require a /me to be removed from a bag or article of clothing.
* Pistols can be lap-carried in a vehicle by all occupants.
* Driver may not shoot while the vehicle is in motion. 
* Pistols can be carried on a motorbike unholstered. 





Shotguns
========

+-----------------------------------------------+-----------------------------------------------+
| **Legal**                                     | **Illegal**                                   |
+-----------------------------------------------+-----------------------------------------------+
| Pump Shotgun MK.2                             | Assault Shotgun                               |
+-----------------------------------------------+-----------------------------------------------+
| Double Barrel                                 | Sawed Off                                     |
+-----------------------------------------------+-----------------------------------------------+
| Pump Shotgun (Police Only)                    | Bullpup Shotgun                               |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Sweeper Shotgun                               |
+-----------------------------------------------+-----------------------------------------------+

* Shotguns cannot be removed from a bag or any article of clothing.
	* Sawn-off is an exception and can be removed from a bag or a large coat.
	* Double Barrel is an exception and can be removed from a bag or a large coat.
* Shotguns can be removed from a house and carried 
* Shotguns must be carried at all times until the weapon is discarded via /me
* Shotguns can be stored in or removed from a vehicle with a /me
	* Ex. /me unracks shotgun.
	* Ex. /me lays the shotgun in the trunk. 
* Shotguns cannot be carried on a motorbike.
	* Sawn-off is an exception and can be carried on a motorbike in a bag or large coat. 
	* Double Barrel is an exception and can be carried on a motorbike in a bag or large coat.
* Shotguns can be lap-carried in a vehicle by passengers.
	* The driver must use a /me to remove the shotgun from the vehicle.





Sub-Machine Guns
================

+-----------------------------------------------+-----------------------------------------------+
| **Legal**                                     | **Illegal**                                   |
+-----------------------------------------------+-----------------------------------------------+
| SMG (Police Only)                             | Combat PDW                                    |
+-----------------------------------------------+-----------------------------------------------+
| SMG MK2 (class3)                              | Mini SMG                                      |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Micro SMG                                     |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Machine Pistol                                |
+-----------------------------------------------+-----------------------------------------------+
|                                               | SMG MK2                                       |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Assault SMG                                   |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Tommy Gun                                     |
+-----------------------------------------------+-----------------------------------------------+

* SMGs can be stored in or removed from a bag or large coat.
	* Mini SMG and Machine Pistol are small enough to be pulled from a hoodie.
* SMGs may be carried on a motorbike. 
* SMGs can be lap-carried in a vehicle by all occupants.
	* Driver may not shoot while the vehicle is in motion.





Assault Rifles
==============

+-----------------------------------------------+-----------------------------------------------+
| **Legal**                                     | **Illegal**                                   |
+-----------------------------------------------+-----------------------------------------------+
| Carbine Rifle (Police Only)                   | Bullpup Rifle MK.2                            |
+-----------------------------------------------+-----------------------------------------------+
| Carbine Rifle MK.2 (Class 3 Only)             | Compact Rifle                                 |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Advanced Rifle                                |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Assault Rifle MK.2                            |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Special Carbine                               |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Special Carbine MK.2                          |
+-----------------------------------------------+-----------------------------------------------+

* Assault Rifles cannot be removed from a bag or any article of clothing.
	* Compact Rifle is an exception and can be removed from a bag.
* Assault Rifles can be removed from a house and remain carried.
* Assault Rifles must be carried at all times until the weapon is discarded via /me
* Assault Rifles can be stored in or removed from a vehicle with a /me
	* Ex. /me unracks AR.
	* Ex. /me lays the AR down in the trunk. 
* Assault Rifles cannot be carried on a motorbike.
	* Compact Rifle is an exception and can be carried on a motorbike in a bag.
* Assault Rifles can be lap-carried in a vehicle by passengers.
	* The driver must use a /me to remove the AR from the vehicle.





Sniper Rifles and MG’s
======================

.. danger:: All sniper usaged from **2019-06-20** are banned from Sunrise Roleplay, if you do have a sniper rifle in your possession please contact an admin and they'll refund the sniper rifle and take it away from you. If you don't want to do that and like to still keep your sniper rifle you may do so, however, you may **NOT** use the sniper in any scenarios. If you are found doing so punishments will be issued based on how large of a scenario you've ruined, and so on.


+-----------------------------------------------+-----------------------------------------------+
| **Legal**                                     | **Illegal**                                   |
+-----------------------------------------------+-----------------------------------------------+
| Sniper Rifle (Police Only)                    | Sniper Rifle                                  |
+-----------------------------------------------+-----------------------------------------------+
| Heavy Sniper (Police Only)                    | Heavy Sniper                                  |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Heavy Sniper MK.2                             |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Machine Gun                                   |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Combat MG                                     |
+-----------------------------------------------+-----------------------------------------------+

* Sniper Rifles and MGs cannot be removed from a bag or any article of clothing.
* Sniper Rifles and MGs can be removed from a house and remain carried.
* Sniper Rifles and MGs must be carried at all times until the weapon is discarded via /me
* Sniper Rifles and MGs can be stored in or removed from a vehicle’s trunk with a /me
	* Ex. /me removes the MG from the trunk.
	* Ex. /me lays the Sniper Rifle down in the trunk. 
* Sniper Rifles and MGs cannot be carried on a motorbike.
* Sniper Rifles and MGs cannot be lap-carried in a vehicle.
	* MGs can be shot from vans that allow it. 





Explosives/Thrown
=================

+-----------------------------------------------+-----------------------------------------------+
| **Legal**                                     | **Illegal**                                   |
+-----------------------------------------------+-----------------------------------------------+
| Flare                                         | Molotov                                       |
+-----------------------------------------------+-----------------------------------------------+
| Flashbang (Police Only)                       | Pipe Bomb                                     |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Grenade                                       |
+-----------------------------------------------+-----------------------------------------------+
|                                               | Sticky Bomb                                   |
+-----------------------------------------------+-----------------------------------------------+

* Can be removed from bags clothing with larger pockets (hoodies or larger) without a /me.
	* Molotovs require a /me to be removed from a bag or article of clothing